const app = require('./server');

app.listen(5000, '0.0.0.0', () => {
    console.log(`App listening on port 5000`);
    console.log(`Press Ctrl+C to quit.`);
});