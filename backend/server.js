const express = require('express');
const cors = require('cors');
const client = require("./connection");

const app = express();
const router = express.Router();

app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: false}));
app.use(cors());

//app.use(routes);

app.get('/', function (req, res) {
    var responseText = 'Server working!';
    res.send(responseText);
});

app.post('/login', async (req, res) => {
    console.info('POST /login');
    const {user, password} = req.body;
    var result;
    try {
        await client.connect();
        const users = await client.db("BD2").collection("User")
        const usr = await users.find({nickname:user, password: password}).toArray();
        result = usr.length === 1 ? true:false;
    } catch (error) {
        console.log(error);
        result = false;
    }
    finally{
        await client.close();
        result === true ? res.status(200).json({success:true}):res.status(500).json({success:false});
    }
});

module.exports = app;