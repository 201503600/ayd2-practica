import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom';

import Login from './Login';

function App() {
  return (
    <Router>
        <div className="App">
          <Routes>
              <Route path="/" element={<div><Login/><h2>201503600</h2></div>} />
          </Routes>
        </div>
    </Router>
  );
}

export default App;
