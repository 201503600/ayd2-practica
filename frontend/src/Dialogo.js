import React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';

class Dialogo extends React.Component {

  handleClose = () => {
    this.props.cerrar();
  }

  render() {
    let {titulo, mensaje, open} = this.props;
    return (
      <Dialog
        open = {open}
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{titulo}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">{mensaje}</DialogContentText>
        </DialogContent>
        
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">OK</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default Dialogo;