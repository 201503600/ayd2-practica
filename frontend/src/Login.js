import * as React from "react";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Link from "@mui/material/Link";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import Alert from "@mui/material/Alert";

import Dialogo from "./Dialogo";

import md5 from "md5";
import "./Login.css";

class Login extends React.Component {
  ruta = {
    ip: process.env.REACT_APP_MIDDLEWARE_IP,
    port: process.env.REACT_APP_MIDDLEWARE_PORT,
  };
  titulo = "";
  mensaje = "";
  state = {
    nickname: "",
    password: "",
    openError: false,
    message: "",
  };

  goInicio = async () => {
    let config = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST, GET",
        "Access-Control-Request-Method": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept, Authorization",
      },
      body: JSON.stringify({
        user: this.state.nickname,
        password: md5(this.state.password),
      }),
      mode: "cors",
    };
    
    try {
      let response = await fetch(
        "http://" + this.ruta.ip + ":" + this.ruta.port + "/login",
        config
      );
      let data = await response.json();
      if (data.success === true) {
        this.setState({ message: {success:true, student: 201503600} });
      } else if (data.success === false) {
        this.abrirError("Credenciales incorrectas!");
        this.setState({
          message: {success:false, student: 201503600}
        });
      } else {
        this.abrirError("Ocurrio un error! Intente de nuevo");
        this.setState({ message: {success:false, student: 201503600}});
      }
    } catch (error) {
      console.log(error);
      this.abrirError("Ocurrio un error! Intente de nuevo");
      this.setState({ message: {success:false, student: 201503600} });
    }
  };

  abrirError = (mensaje) => {
    this.titulo = "ERROR";
    this.mensaje = mensaje;
    this.setState({
      openError: true,
    });
  };

  cerrarError = () => {
    this.setState({
      openError: false,
    });
  };

  setNickname = async (e) => {
    this.state.nickname = e.target.value;
  };

  setPass = async (e) => {
    this.state.password = e.target.value;
  };

  render() {
    return (
      <div>
        <Grid container className="root" spacing={2} justifyContent="center">
          <Paper className="paper" elevation={5} justify="center">
            <div
              style={{
                width: "100%",
                display: "flex",
                // justifyContent: "space-evenly"
              }}
            >
              <div style={{ width: "20%", paddingTop: "2em", display: "flex" }}>
                <Avatar
                  className="img"
                  src=""
                  alt="Travis Howard"
                  style={{ height: "auto", width: "80%" }}
                />
              </div>

              <div style={{ width: "80%" }}>
                <br />
                <TextField
                  label="UserName"
                  inputProps={{ "data-testid": "input-user" }}
                  fullWidth
                  margin="normal"
                  onChange={this.setNickname}
                />
                <TextField
                  label="Password"
                  inputProps={{ "data-testid": "input-password" }}
                  type="password"
                  fullWidth
                  margin="normal"
                  onChange={this.setPass}
                />
              </div>
            </div>
            <br />
            <Grid
              container
              spacing={2}
              justifyContent="center"
              style={{ marginLeft: "6em" }}
            >
              <Button
                variant="outlined"
                data-testid="boton-login"
                color="primary"
                onClick={this.goInicio}
              >
                Login
              </Button>
            </Grid>
            <br />
            <br />
            <br />
            <Grid container spacing={2} justifyContent="center">
              <Typography>
                <Link href="#">
                  No tienes cuenta? Registrate!
                </Link>
              </Typography>
            </Grid>
            <input type="hidden" data-testid="alert" value={this.state.message !== null?JSON.stringify(this.state.message):""}/>
            {this.state.message &&
              (this.state.message.code === 200 ? (
                <Alert severity="success">
                  {this.state.message.code} — {this.state.message.msg}
                </Alert>
              ) : (
                <Alert severity="error">
                  {this.state.message.code} — {this.state.message.msg}
                </Alert>
              ))}
          </Paper>
          <Dialogo
            titulo={this.titulo}
            mensaje={this.mensaje}
            open={this.state.openError}
            cerrar={this.cerrarError}
          />
        </Grid>
      </div>
    );
  }
}

export default Login;
