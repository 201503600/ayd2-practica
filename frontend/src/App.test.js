import React from 'react';
import {
  render, 
  fireEvent, 
  waitFor, 
  cleanup,
  screen
} from '@testing-library/react';

import Login from './Login';

beforeEach(() => {jest.setTimeout(10000)});
afterEach(cleanup);

describe('Login app', () => {
  it('Submit works', async () => {
    render(<Login/>);
    const userInput = screen.getByTestId('input-user');
    const passInput = screen.getByTestId('input-password');
    const submit = screen.getByTestId('boton-login');

    fireEvent.change(userInput,{target:{value:"dan1396"}});
    fireEvent.change(passInput, {target: { value: "123" }});
    fireEvent.click(submit);

    const alert = screen.getByTestId('alert');
    console.log(`Carnet 201503600`);
    await waitFor(() => {
      expect(JSON.parse(alert.getAttribute('value')).success).toBe(true);
      expect(JSON.parse(alert.getAttribute('value')).student).toBe(201503600);
    }, {
      timeout: 10000
    });
    
  }, 10000);
});
